# Puppet Control Repository with Gitlab CICD pipeline

This repository is an example of how to implement a Gitlab CICD pipeline for a Puppet Control Repository, with an emphasis on testing and release. The Pipeline as Code is defined in [*.gitlab-ci.yml*](https://docs.gitlab.com/ee/ci/yaml/).

The [Control Repository](https://puppet.com/docs/pe/latest/code_management/control_repo.html) contains some example roles and profiles which are tested and released in the CI pipeline. 


## Puppet Test coverage
When it comes to testing a typical Puppet Roles/Profiles hierarchy, our opinion is that thoroughly (Black-box + White-box) testing Puppet modules instead of Puppet Roles provides the most value. 
It's too complex to functionally test multiple combinations of Roles, Profiles and target node operation systems, also it's too costly both in testing harness resources and time.

For this reason we aim to perform mainly static analysis and unit tests on the Puppet Control Repository codebase.

For this purpose we find that [*Onceover*](https://github.com/dylanratcliffe/onceover) is a great way to test Control Repo code. It's properly maintained by it's developers and [recommended by Puppet](https://puppet.com/blog/use-onceover-to-start-testing-your-puppet-control-repository/). 


### Static Analysis
The first stage in the Gitlab CI pipeline performs a code quality test with the plugin [*onceover-codequality*](https://github.com/declarativesystems/onceover-codequality) which checks for syntax and linting.

### Unit testing
The main purpose of Onceover is [*spec testing capability*](https://github.com/dylanratcliffe/onceover#configuration) which allows us to define spec tests in [onceover.yaml](https://github.com/dylanratcliffe/onceover#onceoveryaml) and define [*factsets*](https://github.com/dylanratcliffe/onceover#factsets) to define the testing fixtures. 

The great thing about Onceover spec testing is that, based on the test configuration in onceover.yaml, it tries to compile the catalog, before the code is released to the Puppet Master. This means we get our coding errors earlier in the process. 

### Catalog Diff
While this is not a test, it's quite handy to have insight in. The plugin [*octocatalog-diff*](https://github.com/dylanratcliffe/onceover-octocatalog-diff) compiles two versions of each catalog and returns the differences. This is great for ensuring that changed that you were intending to make have had the desired effect and scope.

### r10k deploy
This is a theoratical concept in the pipeline, because it's not implemented yet. The idea is that the release of the code propagation to the puppet master with r10k should be the last thing that happens in the pipeline, after all tests have succeeded.

What's important here is that this release should not be a fire and forget activity, because the r10k deploy job should succeed, for the pipeline to succeed. 

In this pipeline job we envision a microservice that is responsible for r10k deployment through an API that can POST an deployment but also GET the status of the deployment. 
This way we can wait for the r10k deployment to be finished, so that the job can be finished.




